# Disable app armor
aa-teardown


# Upgrade install software
apt update
apt upgrade -y
apt install -y usbmount avahi-daemon docker.io gpsd chrony apparmor-utils

# set aa to complain mode
aa-complain /etc/apparmor.d/*

# set aa to complain mode on boot
cat << EOF > /etc/rc.local
#!/bin/sh -e

# Put all profiles in complain mode.
aa-complain /etc/apparmor.d/*

exit 0
EOF

# make rc.local executable
chmod +x /etc/rc.local

cat << EOF > /etc/default/gpsd
START_DAEMON="true"
USBAUTO="true"
DEVICES="/dev/ttyACM0"
GPSD_OPTIONS="-n"
EOF

cat << EOF > /etc/chrony/chrony.conf
pool ntp.ubuntu.com        iburst maxsources 4
pool 0.ubuntu.pool.ntp.org iburst maxsources 1
pool 1.ubuntu.pool.ntp.org iburst maxsources 1
pool 2.ubuntu.pool.ntp.org iburst maxsources 2
refclock SHM 0  delay 0.5 refid NEMA
keyfile /etc/chrony/chrony.keys
driftfile /var/lib/chrony/chrony.drift
logdir /var/log/chrony
maxupdateskew 100.0
rtcsync
makestep 1 3
EOF


cat << EOF > /etc/avahi/services/skyhub.service
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
  <name replace-wildcards="yes">%h Sky Hub Management UI</name>
  <service>
    <type>_http._tcp</type>
    <port>80</port>
  </service>
</service-group>
EOF

systemctl enable ssh
systemctl enable docker
systemctl enable avahi-daemon
systemctl enable chrony
systemctl restart gpsd
systemctl restart ssh
systemctl restart avahi-daemon
systemctl restart docker
systemctl restart chrony

useradd -G dialout,docker skyhub

cat << EOF >> /etc/sudoers
skyhub	ALL=(ALL:ALL) ALL
EOF

docker volume create portainer_data
docker volume create skyhub_node_data
docker run -d -p 9000:9000 -p 8000:8000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
docker run -d --name watchtower --restart always -v /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower --include-stopped --revive-stopped --interval 30
docker run -d --network host --name skyhub-node --restart always -v skyhub_node_data:/skyhub skyhub/skyhub-node:latest
docker run -d -p 80:80 -p 8088:8088 --name skyhub-node-ui --restart always -v skyhub_node_data:/skyhub skyhub/skyhub-node-ui:latest


#!/bin/sh

cat << EOF > /usr/local/bin/skyhub-restart-node
#!/bin/sh

sudo docker stop skyhub-node
sudo docker rm skyhub-node
sudo docker run -d --name skyhub-node --network host --restart always -v skyhub_node_data:/skyhub skyhub/skyhub-node
EOF

cat << EOF > /usr/local/bin/skyhub-restart-ui
#!/bin/sh

sudo docker stop skyhub-node-ui
sudo docker rm skyhub-node-ui
sudo docker run -d -p 80:80 -p 8088:8088 --name skyhub-node-ui --restart always -v skyhub_node_data:/skyhub skyhub/skyhub-node-ui:latest
EOF

chmod +x /usr/local/bin/skyhub-restart-node-ui /usr/local/bin/skyhub-restart-node

cat << EOF
Welcome to Sky Hub!

Your node is now fully operational.

Set your password by running:

passwd skyhub


Thanks,

Sky Hub Team
EOF
