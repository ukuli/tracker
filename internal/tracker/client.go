/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package tracker

import (
	"crypto/tls"
	"github.com/pkg/errors"
	pb "gitlab.com/skyhuborg/tracker/internal/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/backoff"
	"google.golang.org/grpc/credentials"
	_ "google.golang.org/grpc/encoding/gzip"
	"log"
)

type ClientGRPC struct {
	conn   *grpc.ClientConn
	Client pb.TrackerdClient
}

type ClientGRPCConfig struct {
	Address           string
	RootCertificate   string
	Compress          bool
	AutoTLS           bool
	AutoTLSServerName string
}

func NewClientGRPC(cfg ClientGRPCConfig) (c ClientGRPC, err error) {

	cp := grpc.ConnectParams{
		Backoff: backoff.DefaultConfig,
	}

	var (
		grpcOpts = []grpc.DialOption{
			grpc.WithConnectParams(cp),
		}
		grpcCreds credentials.TransportCredentials
	)

	if cfg.Address == "" {
		err = errors.Errorf("address must be specified")
		return
	}

	if cfg.Compress {
		grpcOpts = append(grpcOpts,
			grpc.WithDefaultCallOptions(grpc.UseCompressor("gzip")))
	}

	if cfg.RootCertificate != "" {
		grpcCreds, err = credentials.NewClientTLSFromFile(cfg.RootCertificate, "localhost")
		if err != nil {
			err = errors.Wrapf(err,
				"failed to create grpc tls client via root-cert %s",
				cfg.RootCertificate)
			return
		}
		grpcOpts = append(grpcOpts, grpc.WithTransportCredentials(grpcCreds))
	} else if cfg.AutoTLS {
		config := &tls.Config{}
		grpcOpts = append(grpcOpts, grpc.WithTransportCredentials(credentials.NewTLS(config)))
	} else {
		grpcOpts = append(grpcOpts, grpc.WithInsecure())
	}

	c.conn, err = grpc.Dial(cfg.Address, grpcOpts...)

	if err == nil {
		log.Printf("Successfully connected to trackerd at %s\n", cfg.Address)
		c.Client = pb.NewTrackerdClient(c.conn)
	}

	return
}

func (c *ClientGRPC) Close() {
	if c.conn != nil {
		c.conn.Close()
	}
}
