/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package sensor

import (
	"log"
	"time"

	"gitlab.com/skyhuborg/tracker/internal/common"
	"gitlab.com/skyhuborg/tracker/internal/event"
	pb "gitlab.com/skyhuborg/tracker/internal/proto"
)

type Sensor interface {
	read() []interface{}
	init() bool
	close() bool
	name() string
}

type Monitor struct {
	sensors             []Sensor
	pollInterval        time.Duration
	channels            []chan *pb.SensorReport
	state               *event.State
	rapidLoggingEnabled bool
}

func NewMonitor(state *event.State) (m Monitor, err error) {
	m.state = state
	m.rapidLoggingEnabled = false
	//m.state.Ref()
	return
}

func (m *Monitor) SetPollInterval(interval time.Duration) {
	m.pollInterval = interval
}

func (m *Monitor) Poll() {
	var (
		activeSensors []string
	)

	for _, s := range m.sensors {
		activeSensors = append(activeSensors, s.name())
	}

	log.Printf("Monitoring sensors: %s\n", activeSensors)

	for range time.Tick(m.pollInterval) {
		var (
			ev *event.Event = m.state.GetEvent()
		)

		isEmpty := true

		report := &pb.SensorReport{}

		if ev.GetInProgress() {
			m.rapidLoggingEnabled = true
			m.pollInterval = 1 * time.Second
		} else {
			if m.rapidLoggingEnabled {
				m.rapidLoggingEnabled = false
				m.pollInterval = 5 * time.Second
			}

		}

		for _, sensor := range m.sensors {
			data := sensor.read()

			if len(data) == 0 {
				continue
			}

			isEmpty = false

			for _, value := range data {

				switch o := value.(type) {
				case *pb.GPS_TPVReport:
					report.GPS_TPVReport = o
				case *pb.GPS_SKYReport:
					report.GPS_SKYReport = o
				case *pb.GPS_GSTReport:
					report.GPS_GSTReport = o
				case *pb.GPS_ATTReport:
					report.GPS_ATTReport = o
				case *pb.GPS_VERSIONReport:
					report.GPS_VERSIONReport = o
				case *pb.GPS_DEVICESReport:
					report.GPS_DEVICESReport = o
				case *pb.GPS_DEVICEReport:
					report.GPS_DEVICEReport = o
				case *pb.GPS_PPSReport:
					report.GPS_PPSReport = o
				case *pb.Time:
					report.Time = o
				case *pb.Accelerometer:
					report.Accelerometer = o
				case *pb.Gyroscope:
					report.Gyroscope = o
				case *pb.Angle:
					report.Angle = o
				case *pb.Magnetometer:
					report.Magnetometer = o
				case *pb.Pressure:
					report.Pressure = o
				case *pb.Altitude:
					report.Altitude = o
				case *pb.LonLat:
					report.LonLat = o
				default:
					log.Printf("Unknown type: %T", o)
				}
			}
		}

		if !isEmpty {
			// populate tracker info here

			report.Tracker = &pb.TrackerInfo{
				Uuid:      common.Info.TrackerId,
				Name:      common.Info.TrackerName,
				Hostname:  common.Info.TrackerHostname,
				BuildId:   common.Info.BuildId,
				BuildDate: common.Info.BuildDate,
				Longitude: common.Info.Longitude,
				Latitude:  common.Info.Latitude,
			}

			// if an event is in progress, store the event id in the report
			if ev.GetInProgress() {
				report.EventId = ev.GetId()
			}

			// dont push sensor report unless we have gps lock
			if common.IsGpsSynchronized() {
				for _, ch := range m.channels {
					ch <- report
				}
			}
		}
	}
}

func (m *Monitor) RegisterChannel(ch chan *pb.SensorReport) {
	m.channels = append(m.channels, ch)
}

func (m *Monitor) Register(s Sensor) {
	if s == nil {
		return
	}

	b := s.init()

	if !b {
		return
	}
	m.sensors = append(m.sensors, s)
}
