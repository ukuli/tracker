/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

package event

import (
	"gitlab.com/skyhuborg/tracker/pkg/db"
	"log"
	"time"
)

type State struct {
	ev          *Event
	lastTrigger time.Time
	refCount    int
	db          *db.DB
	channels    []chan Event
}

func NewState(db *db.DB) *State {
	var (
		state State
	)
	state.ev = NewEvent()
	state.refCount = 0
	state.db = db

	go state.Poll()

	return &state
}

// register channel to get notified of a finished event
func (state *State) RegisterChannel(ch chan Event) {
	state.channels = append(state.channels, ch)
}

func (state *State) GetEvent() *Event {
	return state.ev
}

func (state *State) Ref() {
	state.refCount++
}

func (state *State) Unref() {
	state.refCount--
}

func (state *State) GetRefCount() int {
	return state.refCount
}

func (state *State) Trigger(src *Source) {
	var (
		ev *Event = state.GetEvent()
	)

	if ev.GetInProgress() && !ev.GetIsComplete() {
		state.lastTrigger = time.Now()
	}

	if !ev.GetInProgress() && ev.GetIsComplete() {
		ev.SetWaitGroup(state.refCount)
		ev.Start()
		state.lastTrigger = time.Now()
		state.db.StartEvent(
			ev.GetId(),
			ev.GetStartTime(),
			ev.GetSourceType().String(),
			ev.GetSource().GetName(),
			ev.GetSource().GetSensor())
		log.Printf("Event %s started at %s\n", ev.GetId(), ev.GetStartTime())
	}
}

func (state *State) Poll() {
	for range time.Tick(1 * time.Second) {
		var (
			ev      *Event = state.GetEvent()
			seconds int
		)

		if ev.GetInProgress() {
			elapsed := time.Since(state.lastTrigger)

			seconds = int(elapsed.Seconds())

			if seconds >= 10 {
				ev.Stop()
				state.db.StopEvent(
					ev.GetId(),
					ev.GetEndTime(),
					ev.GetDuration())

				// publish Event
				for _, ch := range state.channels {
					ch <- *ev
				}

				log.Printf("Event %s stopped at %s lasted for %d ms\n",
					ev.GetId(),
					ev.GetEndTime(),
					ev.GetDuration())
			}
		}
	}
}
