package gcs

import (
	"fmt"
	"log"
	"os"
	"path"
	"time"

	"gitlab.com/skyhuborg/tracker/internal/db"
)

type Sync struct {
	config *SyncConfig
	client *Client
	db     *db.DB
}

type SyncConfig struct {
	Concurrency   int
	EnableDevMode bool
	DbUri         string
	Bucket        string
}

func NewSync(config *SyncConfig) *Sync {
	sync := &Sync{
		config: config,
	}
	return sync
}

func (s *Sync) getNextVideoEvent() *db.VideoEvent {
	for {
		videoEvent := s.db.GetNextUnsyncedVideo()

		if videoEvent == nil {
			time.Sleep(time.Second * 10)
			continue
		}

		return videoEvent
	}
	return nil
}

func (s *Sync) Start() {
	s.db = db.NewDB()

	err := s.db.Connect(s.config.DbUri)

	if err != nil {
		log.Fatalf("DB Connect failed: %s\n", err)
		return
	}

	clientConfig := ClientConfig{
		EnableDevMode: s.config.EnableDevMode,
		Bucket:        s.config.Bucket,
	}

	s.client = NewClient(&clientConfig)

	err = s.client.Open()

	if err != nil {
		log.Printf("Sync: Failed connecting to google storage: %s\n", err)
		return
	}

	for {
		videoEvent := s.getNextVideoEvent()

		videoFilename := path.Base(videoEvent.Uri)

		videoDestUri := fmt.Sprintf("%s/video/%s", videoEvent.TrackerId, videoFilename)

		err = s.client.Put(videoDestUri, videoEvent.Uri)

		if err != nil {
			log.Printf("Sync: failed uploading %s with error: %s\n", videoDestUri, err)
		} else {
			log.Printf("Sync: Put %s to %s\n", videoFilename, videoDestUri)
			s.db.SetVideoSynced(videoEvent.EventUuid, videoDestUri)
			e := os.Remove(videoEvent.Uri)
			if e != nil {
				log.Printf("Sync: Error removing video %s: %s", videoEvent.Uri, e)
			} else {
				log.Printf("Sync: Removed video file:%s", videoEvent.Uri)
			}
		}

		thumbnailFilename := path.Base(videoEvent.Thumbnail)

		thumbnailDestUri := fmt.Sprintf("%s/thumbnail/%s", videoEvent.TrackerId, thumbnailFilename)

		err = s.client.Put(thumbnailDestUri, videoEvent.Thumbnail)

		if err != nil {
			log.Printf("Sync: failed uploading %s with error: %s\n", thumbnailDestUri, err)
		} else {
			log.Printf("Sync: Put %s to %s\n", thumbnailFilename, thumbnailDestUri)
			s.db.SetThumbnailSynced(videoEvent.EventUuid, thumbnailDestUri)
			e := os.Remove(videoEvent.Thumbnail)
			if e != nil {
				log.Printf("Sync: Error removing thumbnail %s: %s", videoEvent.Thumbnail, e)
			} else {
				log.Printf("Sync: Removed thumbnail file %s", videoEvent.Thumbnail)
			}
		}

	}
}

func (s *Sync) Stop() {
}
